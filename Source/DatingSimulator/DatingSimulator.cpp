// Copyright Epic Games, Inc. All Rights Reserved.

#include "DatingSimulator.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DatingSimulator, "DatingSimulator" );
